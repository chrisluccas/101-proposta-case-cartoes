package com.exemplo.pagamento.exemplopa.creditcards.models.dto

class RequestChangeActivationCreditCard (val activation: Boolean)