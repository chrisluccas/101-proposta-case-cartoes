package com.exemplo.pagamento.exemplopa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExemplopaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExemplopaApplication.class, args);
	}

}
