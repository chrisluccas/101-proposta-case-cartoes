package com.exemplo.pagamento.exemplopa.creditcards.models.dto

class ResponseChangeActivationCreditCard (val id: Long?, val number: String, val clientId: Long?, val activate: Boolean)