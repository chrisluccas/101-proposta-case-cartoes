package com.exemplo.pagamento.exemplopa.payments.models;

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard;

import javax.persistence.*;

@Entity
@Table(name = "payments", schema = "new_schema")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @ManyToOne
    private CreditCard creditCard;

    @Column(name = "value")
    private double value;

    @Column(name = "description")
    private String description;

    public Payment() {}

    public long getId() {
        return id;
    }

    public double getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
