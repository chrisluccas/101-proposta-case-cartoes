package com.exemplo.pagamento.exemplopa.creditcards.service;

import com.exemplo.pagamento.exemplopa.clients.service.ClientService;
import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard;
import com.exemplo.pagamento.exemplopa.clients.repository.ClientRepository;
import com.exemplo.pagamento.exemplopa.creditcards.models.exceptions.CreditCardNotFoundException;
import com.exemplo.pagamento.exemplopa.creditcards.repository.CreditCardRepository;
import com.exemplo.pagamento.exemplopa.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CreditCardService implements IService<CreditCard> {

    @Autowired
    private CreditCardRepository repository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientService clientService;

    public CreditCardService(CreditCardRepository repository, ClientRepository clientRepository) {
        this.repository = repository;
        this.clientRepository = clientRepository;
    }

    @Override
    public List<CreditCard> getAll() {
        return null;
    }

    @Override
    public CreditCard get(long id) {
        return null;
    }

    public CreditCard get(String number) {
        Optional<CreditCard> optional = this.repository.getByNumber(number);
        if (!optional.isPresent())
            throw new CreditCardNotFoundException();
        return optional.get();
    }

    @Override
    public CreditCard save(CreditCard creditCard) {
        if (clientService.get(creditCard.getClient().getId()) != null) {
            try {
                throw new Exception("Usuário nâo encontrado");
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }
            return null;
        }
        else {
            return this.repository.save(creditCard);
        }
    }

    @Override
    public void delete(long id) {}

    @Override
    public CreditCard update(CreditCard model) {
        return null;
    }

    public CreditCard update(String number, boolean activate) {
        return this.repository.updateByNumber(number, activate);
    }

    @Override
    public CreditCard updateById(long id, CreditCard model) {
        return null;
    }
}
