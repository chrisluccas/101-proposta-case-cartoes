package com.exemplo.pagamento.exemplopa.creditcards.models.mappers

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.RequestChangeActivationCreditCard
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.RequestCreditCard
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.ResponseChangeActivationCreditCard
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.ResponseCreditCard

object CreditCardMapper {

    @JvmStatic
    fun toCreditCard(requestCreditCard: RequestCreditCard) : CreditCard {
        return CreditCard().apply {
            this.number = requestCreditCard.number
            this.isActivate = false
        }
    }

    @JvmStatic
    fun toRequestChangeActivationCreditCard(creditCard: CreditCard)
            : ResponseChangeActivationCreditCard = ResponseChangeActivationCreditCard(creditCard.id
            , creditCard.number
            , creditCard.client.id
            , creditCard.isActivate)

    @JvmStatic
    fun toResponseCreditCard(creditCard: CreditCard) : ResponseCreditCard =
            ResponseCreditCard(creditCard.id
                    , creditCard.number
                    , creditCard.client.id
                    , creditCard.isActivate)

}