package com.exemplo.pagamento.exemplopa.payments.models.dto

class ResponsePayment(val id: Long?, val cardId: Long?, val description: String, val value: Double)