package com.exemplo.pagamento.exemplopa.creditcards.models.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class RequestCreditCard (@NotBlank @Size(min=3, max = 9) val number: String, @NotNull val clientId: Long)