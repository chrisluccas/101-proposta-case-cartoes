package com.exemplo.pagamento.exemplopa.payments.models.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class RequestPayment(@NotNull val cardId: Long
                     , @NotBlank @Size(min = 3) val description: String
                     , @NotNull val value: Double)