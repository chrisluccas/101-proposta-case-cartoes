package com.exemplo.pagamento.exemplopa.clients.models.mappers

import com.exemplo.pagamento.exemplopa.clients.models.Client
import com.exemplo.pagamento.exemplopa.clients.models.dto.DetailedResponseClient
import com.exemplo.pagamento.exemplopa.clients.models.dto.RequestClient
import com.exemplo.pagamento.exemplopa.clients.models.dto.ResponseClient

object ClientMapper {

    @JvmStatic
    fun toClient(requestClient: RequestClient) : Client {
        return Client().apply { this.name = requestClient.name }
    }

    @JvmStatic
    fun fromResponseClient(client: Client) : ResponseClient {
        return ResponseClient(client.id, client.name)
    }

    @JvmStatic
    fun toDetailedResponseClient(client: Client): DetailedResponseClient = DetailedResponseClient(client.id, client.name)
}