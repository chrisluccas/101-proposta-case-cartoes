package com.exemplo.pagamento.exemplopa.clients.repository

import com.exemplo.pagamento.exemplopa.clients.models.Client
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface ClientRepository: JpaRepository<Client, Long> {
    fun getById(id: Long) : Client
}