package com.exemplo.pagamento.exemplopa.creditcards.models;

import com.exemplo.pagamento.exemplopa.clients.models.Client;

import javax.persistence.*;

@Entity
@Table(name = "cards", schema = "new_schema")
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Client client;

    @Column(name = "activate")
    private boolean activate;

    @Column(name = "number")
    private String number;

    public CreditCard() { }

    public long getId() {
        return id;
    }

    public boolean isActivate() {
        return activate;
    }

    public String getNumber() {
        return number;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Client getClient() {
        return client;
    }
}
