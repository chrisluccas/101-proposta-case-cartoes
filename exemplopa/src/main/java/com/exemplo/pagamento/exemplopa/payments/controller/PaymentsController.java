package com.exemplo.pagamento.exemplopa.payments.controller;

import com.exemplo.pagamento.exemplopa.payments.models.Payment;
import com.exemplo.pagamento.exemplopa.payments.models.dto.RequestPayment;
import com.exemplo.pagamento.exemplopa.payments.models.dto.ResponsePayment;
import com.exemplo.pagamento.exemplopa.payments.models.mappers.PaymentMapper;
import com.exemplo.pagamento.exemplopa.payments.service.PaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
public class PaymentsController {

    @Autowired
    private PaymentsService service;

    public PaymentsController(PaymentsService service) {
        this.service = service;
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/pagamento")
    public ResponsePayment create(@RequestBody RequestPayment requestPayment) {
        Payment payment = PaymentMapper.toPayment(requestPayment);
        return PaymentMapper.toResponsePayment(this.service.save(payment));
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<ResponsePayment> get(@PathVariable(name="id_cartao") long id) {
        List<Payment> payments = this.service.getAllByCardId(id);
        return payments.stream()
                .map(PaymentMapper::toResponsePayment)
                .collect(Collectors.toList());
    }

}
