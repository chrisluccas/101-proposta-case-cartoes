package com.exemplo.pagamento.exemplopa.payments.repository;


import com.exemplo.pagamento.exemplopa.payments.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface PaymentsRepository extends JpaRepository<Payment, Long> {

   // @Query("SELECT c FROM cards c WHERE c.id:=id")
    List<Payment> getByIdCard(@Param("id") long id);
}
