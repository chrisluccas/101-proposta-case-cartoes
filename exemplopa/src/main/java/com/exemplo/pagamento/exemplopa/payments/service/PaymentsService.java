package com.exemplo.pagamento.exemplopa.payments.service;

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard;
import com.exemplo.pagamento.exemplopa.payments.models.Payment;
import com.exemplo.pagamento.exemplopa.creditcards.repository.CreditCardRepository;
import com.exemplo.pagamento.exemplopa.payments.repository.PaymentsRepository;
import com.exemplo.pagamento.exemplopa.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentsService implements IService<Payment> {

    @Autowired
    private PaymentsRepository paymentsRepository;

    @Autowired
    private CreditCardRepository creditCardRepository;

    public PaymentsService(PaymentsRepository paymentsRepository, CreditCardRepository creditCardRepository) {
        this.paymentsRepository = paymentsRepository;
        this.creditCardRepository = creditCardRepository;
    }

    @Override
    public List<Payment> getAll() {
        return null;
    }

    public List<Payment> getAllByCardId(long id) {
        return paymentsRepository.getByIdCard(id);
    }

    @Override
    public Payment get(long id) {
        return null;
    }

    @Override
    public Payment save(Payment payment) {
        CreditCard creditCard = creditCardRepository.findById(payment.getCreditCard().getId()).orElseGet(CreditCard::new);
        if (creditCard.isActivate()) {
            return this.paymentsRepository.saveAndFlush(payment);
        }
        else {
            try {
                throw new Exception("Cartâo nâo existe ou não foi ativado");
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }
            return null;
        }
    }

    @Override
    public void delete(long id) { }

    @Override
    public Payment update(Payment model) {
        return null;
    }

    @Override
    public Payment updateById(long id, Payment model) {
        return null;
    }
}
