package com.exemplo.pagamento.exemplopa.clients.models.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class RequestClient(@NotBlank @Size(min = 3)  val name: String)