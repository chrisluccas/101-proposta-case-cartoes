package com.exemplo.pagamento.exemplopa.services;

import java.util.List;

public interface IService<T> {
    public List<T> getAll();

    public T get(long id);

    public T save(T model);

    public void delete(long id);

    public T update(T model);

    public T updateById(long id, T model);
}
