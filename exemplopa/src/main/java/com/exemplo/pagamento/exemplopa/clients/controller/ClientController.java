package com.exemplo.pagamento.exemplopa.clients.controller;

import com.exemplo.pagamento.exemplopa.clients.models.Client;
import com.exemplo.pagamento.exemplopa.clients.models.dto.DetailedResponseClient;
import com.exemplo.pagamento.exemplopa.clients.models.dto.RequestClient;
import com.exemplo.pagamento.exemplopa.clients.models.dto.ResponseClient;
import com.exemplo.pagamento.exemplopa.clients.models.mappers.ClientMapper;
import com.exemplo.pagamento.exemplopa.clients.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientController {

    @Autowired
    private ClientService service;

    public ClientController(ClientService service) {
        this.service = service;
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/client")
    public ResponseClient add(@RequestBody RequestClient requestClient) {
        Client client = ClientMapper.toClient(requestClient);
        return ClientMapper.fromResponseClient(this.service.save(client));
    }

    @GetMapping("/client/{id}")
    public DetailedResponseClient get(@PathVariable(name="id") long id) {
        return ClientMapper.toDetailedResponseClient(this.service.get(id));
    }

}
