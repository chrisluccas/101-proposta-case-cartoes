package com.exemplo.pagamento.exemplopa.creditcards.controller;

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard;
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.RequestChangeActivationCreditCard;
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.RequestCreditCard;
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.ResponseChangeActivationCreditCard;
import com.exemplo.pagamento.exemplopa.creditcards.models.dto.ResponseCreditCard;
import com.exemplo.pagamento.exemplopa.creditcards.models.mappers.CreditCardMapper;
import com.exemplo.pagamento.exemplopa.creditcards.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CreditCardController {

    @Autowired
    private CreditCardService service;

    public CreditCardController(CreditCardService service) {
        this.service = service;
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/cartao")
    public ResponseCreditCard create(@Valid @RequestBody RequestCreditCard requestCreditCard) {
        CreditCard creditCard = CreditCardMapper.toCreditCard(requestCreditCard);
        creditCard = this.service.save(creditCard);
        return CreditCardMapper.toResponseCreditCard(creditCard);
    }

    @PutMapping("/cartao/{numero}")
    public ResponseChangeActivationCreditCard update(
            @RequestBody RequestChangeActivationCreditCard rqChangeActivationCreditCard
            , @PathVariable(name="numero") String number) {

        CreditCard creditCard = this.service.update(number
                , rqChangeActivationCreditCard.getActivation());

        return CreditCardMapper.toRequestChangeActivationCreditCard(creditCard);
    }

    @GetMapping("/cartao/{numero}")
    public ResponseCreditCard get(@PathVariable(name="numero")  String number) {
        return CreditCardMapper.toResponseCreditCard(this.service.get(number));
    }

}
