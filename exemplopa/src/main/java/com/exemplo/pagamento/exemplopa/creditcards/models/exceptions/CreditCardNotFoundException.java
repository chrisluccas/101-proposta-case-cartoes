package com.exemplo.pagamento.exemplopa.creditcards.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente nâo encontrado")
public class CreditCardNotFoundException extends RuntimeException {}
