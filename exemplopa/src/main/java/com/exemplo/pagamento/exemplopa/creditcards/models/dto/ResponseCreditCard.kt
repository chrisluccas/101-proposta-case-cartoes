package com.exemplo.pagamento.exemplopa.creditcards.models.dto

class ResponseCreditCard(val id: Long?, val number: String, val clientId: Long?, val activate: Boolean)