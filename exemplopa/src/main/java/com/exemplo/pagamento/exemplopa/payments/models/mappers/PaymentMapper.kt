package com.exemplo.pagamento.exemplopa.payments.models.mappers

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard
import com.exemplo.pagamento.exemplopa.payments.models.Payment
import com.exemplo.pagamento.exemplopa.payments.models.dto.RequestPayment
import com.exemplo.pagamento.exemplopa.payments.models.dto.ResponsePayment

object PaymentMapper {

    @JvmStatic
    fun toPayment(requestPayment: RequestPayment) : Payment =
            Payment().apply {
                this.creditCard = CreditCard().apply {  requestPayment.cardId }
                this.value = requestPayment.value
                this.description = requestPayment.description
            }

    @JvmStatic
    fun toResponsePayment(payment: Payment) : ResponsePayment =
            ResponsePayment(payment.id, payment.creditCard.id, payment.description, payment.value)

}