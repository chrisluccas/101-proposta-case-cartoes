package com.exemplo.pagamento.exemplopa.clients.models;

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard;
import com.exemplo.pagamento.exemplopa.payments.models.Payment;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "clients", schema = "new_schema")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    //@JoinColumn(name = "id_client")
    private List<CreditCard> creditCards;

    @OneToMany(cascade = CascadeType.ALL)
    //@JoinColumn(name = "id") // credit card id
    private List<Payment> payments;

    public Client() {}

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreditCards(List<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
}
