package com.exemplo.pagamento.exemplopa.creditcards.repository

import com.exemplo.pagamento.exemplopa.creditcards.models.CreditCard
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface CreditCardRepository : JpaRepository<CreditCard, Long> {

    @Modifying
    @Query("UPDATE CreditCard c set c.activate=:activate WHERE c.number=:number")
    fun updateByNumber(@Param("number") number: String, @Param("activate") activate: Boolean) : CreditCard

    @Modifying
    @Query("SELECT c FROM CreditCard c WHERE c.number=:number")
    fun selectByNumber(@Param("number") number: String) : CreditCard

    fun getByNumber(number: String) : Optional<CreditCard>

}