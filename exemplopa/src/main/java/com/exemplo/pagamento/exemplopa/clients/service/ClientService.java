package com.exemplo.pagamento.exemplopa.clients.service;

import com.exemplo.pagamento.exemplopa.clients.models.Client;
import com.exemplo.pagamento.exemplopa.clients.repository.ClientRepository;
import com.exemplo.pagamento.exemplopa.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService implements IService<Client> {

    @Autowired
    private ClientRepository repository;

    public ClientService(ClientRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Client> getAll() {
        return repository.findAll();
    }

    @Override
    public Client get(long id) {
        return repository.getById(id);
    }

    @Override
    public Client save(Client client) { return this.repository.saveAndFlush(client); }

    @Override
    public void delete(long id) {}

    @Override
    public Client update(Client model) {
        return null;
    }

    @Override
    public Client updateById(long id, Client model) {
        return null;
    }
}
